package id.co.indivara.jdt12.warehousing;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.TransferWTW;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TransferWTWControllerTest {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void transferWTWTSuccessTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        //inputan buat yang ngirim
        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseCode("wrh1");

        //inputan buat yg nerima
        Warehouse warehouse1 = new Warehouse();
        warehouse1.setWarehouseCode("wrh2");

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseCode("mrc1");

        TransferWTW transferWTW = new TransferWTW();
        transferWTW.setStock(500);

        mockMvc.perform(
                        post("/transferwtw/{warehouseCodeSource}/{merchandiseCode}/{warehouseCodeDestination}",warehouse.getWarehouseCode(),merchandise.getMerchandiseCode(),warehouse1.getWarehouseCode())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseCode()))
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseCode()))
                                .content(objectMapper.writeValueAsString(warehouse1.getWarehouseCode()))
                                .content(objectMapper.writeValueAsString(transferWTW))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transferWTWId").exists())
                .andExpect(jsonPath("$.transferWTWCode").exists())
                .andExpect(jsonPath("$.warehouseSource.warehouseCode").value("wrh1")) //yg ngirim
                .andExpect(jsonPath("$.merchandiseCode.merchandiseCode").value("mrc1")) //yg nerima
                .andExpect(jsonPath("$.warehouseDestination.warehouseCode").value("wrh2"))
                .andExpect(jsonPath("$.stock").value(500)) //jumlah transfer
                .andExpect(jsonPath("$.timestamp").exists());

        mockMvc.perform(get("/view/warehouseinventory/warehouse/{warehouseCode}", warehouse.getWarehouseCode())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk());
               // .andExpect(jsonPath("$.[0]stock").value(500)); //expect di tempat yg ngirim bakal berkurang jadi berapa

        mockMvc.perform(get("/view/warehouseinventory/warehouse/{warehouseCode}", warehouse1.getWarehouseCode())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk());
                //.andExpect(jsonPath("$.[0]stock").value(500)); //expect di tempat yg nerima bakal nambah jadi berapa
    }

    @Test
    public void transferWTWTFailedInternalServerErrorTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        //inputan buat yang ngirim
        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseCode("wrh1000");

        //inputan buat yg nerima
        Warehouse warehouse1 = new Warehouse();
        warehouse1.setWarehouseCode("wrh2");

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseCode("mrc1");

        TransferWTW transferWTW = new TransferWTW();
        transferWTW.setStock(500);

        mockMvc.perform(
                        post("/transferwtw/{warehouseCodeSource}/{merchandiseCode}/{warehouseCodeDestination}",warehouse.getWarehouseCode(),merchandise.getMerchandiseCode(),warehouse1.getWarehouseCode())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseCode()))
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseCode()))
                                .content(objectMapper.writeValueAsString(warehouse1.getWarehouseCode()))
                                .content(objectMapper.writeValueAsString(transferWTW))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void transferWTWTBadRequestTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        //inputan buat yang ngirim
        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseCode("wrh1");

        //inputan buat yg nerima
        Warehouse warehouse1 = new Warehouse();
        warehouse1.setWarehouseCode("wrh2");

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseCode("mrc1");

        TransferWTW transferWTW = new TransferWTW();
        transferWTW.setStock(500000);

        mockMvc.perform(
                        post("/transferwtw/{warehouseCodeSource}/{merchandiseCode}/{warehouseCodeDestination}",warehouse.getWarehouseCode(),merchandise.getMerchandiseCode(),warehouse1.getWarehouseCode())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseCode()))
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseCode()))
                                .content(objectMapper.writeValueAsString(warehouse1.getWarehouseCode()))
                                .content(objectMapper.writeValueAsString(transferWTW))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isBadRequest());
    }
}
