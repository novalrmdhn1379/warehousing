package id.co.indivara.jdt12.warehousing;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.repo.MerchandiseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MerchandiseControllerTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    MerchandiseRepository merchandiseRepository;

    @Test
    public void merchandiseCreateTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseName("kaos");

        mockMvc.perform(
                        post("/create/merchandise")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(merchandise))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.merchandiseId").exists())
                .andExpect(jsonPath("$.merchandiseCode").exists())
                .andExpect(jsonPath("$.merchandiseName").value("kaos"));
    }

    @Test
    public void merchandiseCreateFailedUnauthorizedTest() throws Exception {


        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseName("kaos");

        mockMvc.perform(
                        post("/create/merchandise")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(merchandise)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void merchandiseCreateFailedNotFoundTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseName("kaos");

        mockMvc.perform(
                        post("/create/merchandisee")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(merchandise))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isNotFound());
    }

    @Test
    public void merchandiseCreateFailedBadRequestTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        mockMvc.perform(
                        post("/create/merchandise")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void merchandiseFindTest() throws Exception {
        mockMvc.perform(
                        get("/find/merchandise/{merchandiseId}","mrc1")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.merchandiseId").exists())
                .andExpect(jsonPath("$.merchandiseCode").exists())
                .andExpect(jsonPath("$.merchandiseName").value("kaos"));
    }
}
